# ------------------------------------------------------------------------------
# Word Embeddings

#' 160k English-language SGNS word embeddings trained on the British National Corpus
#'
#' SGNS embeddings trained on the British National Corpus. Each term is also
#' tagged with part-of-speech, e.g. "hyperventilation_NOUN"
#' 
#' @references
#' Tomas Mikolov, Kai Chen, Greg Corrado, and Jeffrey Dean. 2013.
#' "Efficient Estimation of Word Representations in Vector Space.""
#' In Proceedings of Workshop at ICLR
#'
#' @docType data
#' @name vecs_sgns300_bnc_pos
#' @keywords datasets
#' @format A matrix of 163,473 rows and 300 columns
#' @source http://vectors.nlpl.eu/repository/
NULL


# ------------------------------------------------------------------------------
# GloVe Embeddings

#' 400k English-language GloVe word embeddings (300 dimensions)
#'
#' A matrix of 400 thousand word vectors (rows) of 300
#' dimensions (columns). Embeddings were trained on the
#' 2014 Wikipedia and Gigaword 5 corpora (total of 6 
#' billion tokens).
#'
#' @references
#' Pennington, Jeffrey, Richard Socher, and Christopher
#' Manning. (2014). "Glove: Global Vectors for Word
#' Representation." Pp. 1532--43 in Proceedings of the
#' 2014 conference on empirical methods in natural
#' language processing
#'
#' @docType data
#' @name vecs_glove300_wiki_gigaword
#' @keywords datasets
#' @format A matrix of 400 thousand rows and 300 columns
#' @usage data("vecs_glove300_wiki_gigaword")
#' @source https://nlp.stanford.edu/projects/glove/
NULL


# ------------------------------------------------------------------------------
# fastText Embeddings


#' 1 million English-language fastText word embeddings
#'
#' A matrix of roughly 1 million word vectors (rows) or 300
#' dimensions (columns). Embeddings were trained on Wikipedia 2017,
#' UMBC webbase corpus 2007 and statmt.org news dataset
#'
#' @docType data
#' @name vecs_fasttext300_wiki_news
#' @keywords datasets
#' @format A matrix of 999,993 rows and 300 columns
#' @usage data("vecs_fasttext300_wiki_news")
#' @source https://fasttext.cc/docs/en/english-vectors.html
NULL

#' 1 million English-language fastText word embeddings, w/subword information
#'
#' A matrix of roughly 1 million word vectors (rows) and 300
#' dimensions (columns). Embeddings were trained trained with subword infomation
#' on Wikipedia 2017, UMBC webbase corpus 2007 and statmt.org news dataset
#'
#' @docType data
#' @name vecs_fasttext300_wiki_news_subword
#' @keywords datasets
#' @format A matrix of 999,993 rows and 300 columns
#' @usage data("vecs_fasttext300_wiki_news_subword")
#' @source https://fasttext.cc/docs/en/english-vectors.html
NULL

#' 2 million English-language fastText word embeddings
#'
#' A matrix of roughly 2 million word vectors (rows) and 300
#' dimensions (columns). Embeddings were trained on the Common Crawl.
#'
#' @docType data
#' @name vecs_fasttext300_commoncrawl
#' @keywords datasets
#' @format A matrix of 2 million rows and 300 columns
#' @source https://fasttext.cc/docs/en/english-vectors.html
NULL

#' 3 million English-language CBOW word embeddings trained on Google News corpus
#'
#' CBOW embeddings trained Google News dataset (corpus of about 
#' 100 billion words). Note: this model is well-known, but 
#' actual model training parameters are not obvious. The paper 
#' implies Skip-Gram, however, is likely CBOW with negative sampling. 
#' As described in the Google Group conversation, Mikolov states "We have 
#' released additional word vectors trained on about 100 billion words 
#' from Google News. The training was performed using the continuous
#' bag of words architecture, with sub-sampling using threshold 1e-5,
#' and with negative sampling with 3 negative examples per each 
#' positive one. The training time was about 9 hours on multi-core machine, 
#' and the resulting vectors have dimensionality 300. Vocabulary size is
#' 3 million, and the entities contain both words and 
#' automatically derived phrases."
#' 
#' Mikolov goes on:
#' 
#' Additional info someone asked for recently - this model was trained 
#' using the following command line: 
#' 
#' ./word2vec -train train100B.txt -read-vocab voc -output 
#'  vectors.bin -cbow 1 -size 300 -window 5 -negative 3 -hs 0 
#' -sample 1e-5 -threads 12 -binary 1 -min-count 10
#' 
#' Source: https://groups.google.com/g/word2vec-toolkit/c/lxbl_MB29Ic/m/NDLGId3KPNEJ
#' 
#'
#' @references
#' Tomas Mikolov, Kai Chen, Greg Corrado, and Jeffrey Dean. 2013.
#' "Efficient Estimation of Word Representations in Vector Space."
#' In Proceedings of Workshop at ICLR
#' 
#' Kozlowski, A. C., Taddy, M., & Evans, J. A. (2019). 
#' "The geometry of culture: Analyzing the meanings of 
#' class through word embeddings."
#' American Sociological Review, 84(5), 905-949.
#'
#' @docType data
#' @name vecs_cbow300_googlenews
#' @keywords datasets
#' @format A matrix of 3 million rows and 300 columns
#' @source https://code.google.com/archive/p/word2vec/
NULL

#' 1 million English-language SGNS word embeddings trained on Google N-Grams
#'
#' SGNS embeddings trained Google Books N-Grams from 2000-2012 using 5-grams.
#' The result is a matrix of 928,250 word vectors and 300 dimensions. All
#' n-grams were lowercased "to increase the frequency of rare words."
#' 
#' Kozlowski et al. explains:
#' 
#' For contemporary validation, we train an embedding model on 
#' Google Ngrams of publications dating from 2000 through 2012.
#' We use this range of years because Google Ngrams do not include
#' publications more recent than 2012, and this duration is similar
#' to those used in our historical analyses 
#'
#' @references
#' Kozlowski, A. C., Taddy, M., & Evans, J. A. (2019). 
#' "The geometry of culture: Analyzing the meanings of 
#' class through word embeddings."
#' American Sociological Review, 84(5), 905-949.
#'
#' @docType data
#' @name vecs_sgns300_googlengrams_kte_en
#' @keywords datasets
#' @format A matrix of 1 million rows and 300 columns
#' @source https://github.com/KnowledgeLab/GeometryofCulture
NULL

# -----------------------------------------------------------------------------
# Diachronic Embeddings

#' 50k diachronic English-language SGNS word embeddings over 20 decades
#'
#' 50 thousand SGNS embeddings from the HistWords project
#' trained on the Corpus of Historical American English divided
#'  into decades. This is a list of 20 elements, in which
#' every element is an embedding matrix associated with a given
#' decade, 1810-2000. Each matrix is 50 thousand vectors (rows) and
#' 300 dimensions (columns). Note that each embedding has the same
#' vocabulary, but when words do not appear in a given decade they
#' appear as rows with only zero values.
#'
#' @references
#' Hamilton, William L., Jure Leskovec, and Dan Jurafsky. 2016.
#' "Diachronic Word Embeddings Reveal Statistical Laws of Semantic Change."
#' Pp. 1489--1501 in Proceedings of the 54th Annual Meeting 
#' of the Association for Computational Linguistics.
#'
#' @docType data
#' @name vecs_sgns300_coha_histwords
#' @keywords datasets
#' @format A list of 20 matrices
#' @source https://nlp.stanford.edu/projects/histwords/
#' 
#' @examples
#' 
#' \dontrun{
#' 
#' ## download the model (once per machine)
#' download_pretrained("vecs_sgns300_coha_histwords")
#' 
#' ## load the model each session
#' data("vecs_sgns300_coha_histwords")
#' 
#' ## check dims
#' length(vecs_sgns300_coha_histwords) == 20L
#' dim(vecs_sgns300_coha_histwords[[1]]) == c(50000, 300)
#' 
#' }
#'
NULL


#' 100k diachronic English-language SGNS word embeddings, 20 decades, Google Books corpus
#'
#' 100 thousand SGNS embeddings from the HistWords project,
#' trained on the Google Books N-Gram corpus (all English) divided 
#' into decades. This is a list of 20 elements, in which every
#'  element is an embedding matrix associated with a given
#' decade, 1800-1990. Each matrix is 100 thousand vectors (rows) and
#' 300 dimensions (columns). Note that each embedding has the same
#' vocabulary, but when words do not appear in a given decade they
#' appear as rows with only zero values.
#'
#' @references
#' Hamilton, William L., Jure Leskovec, and Dan Jurafsky. 2016.
#' "Diachronic Word Embeddings Reveal Statistical Laws of Semantic Change."
#' Pp. 1489--1501 in Proceedings of the 54th Annual Meeting 
#' of the Association for Computational Linguistics.
#'
#' @docType data
#' @name vecs_sgns300_googlengrams_histwords
#' @keywords datasets
#' @format A list of 20 matrices
#' @source https://nlp.stanford.edu/projects/histwords/
#' 
#' @examples
#' 
#' \dontrun{
#' 
#'
#' ## download the model (once per machine)
#' download_pretrained("vecs_sgns300_googlengrams_histwords")
#' 
#' ## load the model each session
#' data("vecs_sgns300_googlengrams_histwords")
#' 
#' ## check dims
#' length(vecs_sgns300_googlengrams_histwords) == 20L
#' dim(vecs_sgns300_googlengrams_histwords[[1]]) == c(100000, 300)
#' 
#' }
#'
NULL


#' 100k diachronic English-language SGNS word embeddings, 20 decades, Google Books Fiction corpus
#'
#' 100 thousand SGNS embeddings from the HistWords project,
#' trained on the Google Books N-Gram Fiction corpus
#' (all English) divided into decades. 
#' This is a list of 20 elements, in which every element is an 
#' embedding matrix associated with a given decade, 1800-1990. 
#' Each matrix is 100 thousand vectors (rows) and 300 dimensions
#' (columns). Note that each embedding has the same vocabulary, 
#' but when words do not appear in a given decade they
#' appear as rows with only zero values.
#'
#' @references
#' Hamilton, William L., Jure Leskovec, and Dan Jurafsky. 2016.
#' "Diachronic Word Embeddings Reveal Statistical Laws of Semantic Change."
#' Pp. 1489--1501 in Proceedings of the 54th Annual Meeting 
#' of the Association for Computational Linguistics.
#'
#' @docType data
#' @name vecs_sgns300_googlengrams_fic_histwords
#' @keywords datasets
#' @format A list of 20 matrices
#' @source https://nlp.stanford.edu/projects/histwords/
#' 
#' @examples
#' 
#' \dontrun{
#' 
#'
#' ## download the model (once per machine)
#' download_pretrained("vecs_sgns300_googlengrams_fic_histwords")
#' 
#' ## load the model each session
#' data("vecs_sgns300_googlengrams_fic_histwords")
#' 
#' ## check dims
#' length(vecs_sgns300_googlengrams_fic_histwords) == 20L
#' dim(vecs_sgns300_googlengrams_fic_histwords[[1]]) == c(100000, 300)
#' 
#' }
#'
NULL

#' 100k diachronic French-language SGNS word embeddings, 20 decades, Google Books corpus
#'
#' 100 thousand SGNS embeddings from the HistWords project,
#' trained on the Google Books N-Gram corpus
#' (all French) divided into decades.
#' This is a list of 20 elements, in which every element is an
#' embedding matrix associated with a given decade, 1800-1990.
#' Each matrix is 100 thousand vectors (rows) and 300 dimensions
#' (columns). Note that each embedding has the same vocabulary,
#' but when words do not appear in a given decade they
#' appear as rows with only zero values.
#'
#' @references
#' Hamilton, William L., Jure Leskovec, and Dan Jurafsky. 2016.
#' "Diachronic Word Embeddings Reveal Statistical Laws of Semantic Change."
#' Pp. 1489--1501 in Proceedings of the 54th Annual Meeting
#' of the Association for Computational Linguistics.
#'
#' @docType data
#' @name vecs_sgns300_googlengrams_histwords_fr
#' @keywords datasets
#' @format A list of 20 matrices
#' @source https://nlp.stanford.edu/projects/histwords/
#' 
#' @examples
#' 
#' \dontrun{
#' 
#'
#' ## download the model (once per machine)
#' download_pretrained("vecs_sgns300_googlengrams_histwords_fr")
#' 
#' ## load the model each session
#' data("vecs_sgns300_googlengrams_histwords_fr")
#' 
#' ## check dims
#' length(vecs_sgns300_googlengrams_histwords_fr) == 20L
#' dim(vecs_sgns300_googlengrams_histwords_fr[[1]]) == c(100000, 300)
#' 
#' }
#'
NULL


#' 100k diachronic German-language SGNS word embeddings, 20 decades, Google Books corpus
#'
#' 100 thousand SGNS embeddings from the HistWords project,
#' trained on the Google Books N-Gram corpus
#' (all German) divided into decades.
#' This is a list of 20 elements, in which every element is an
#' embedding matrix associated with a given decade, 1800-1990.
#' Each matrix is 100 thousand vectors (rows) and 300 dimensions
#' (columns). Note that each embedding has the same vocabulary,
#' but when words do not appear in a given decade they
#' appear as rows with only zero values.
#'
#' @references
#' Hamilton, William L., Jure Leskovec, and Dan Jurafsky. 2016.
#' "Diachronic Word Embeddings Reveal Statistical Laws of Semantic Change."
#' Pp. 1489--1501 in Proceedings of the 54th Annual Meeting
#' of the Association for Computational Linguistics.
#'
#' @docType data
#' @name vecs_sgns300_googlengrams_histwords_de
#' @keywords datasets
#' @format A list of 20 matrices
#' @source https://nlp.stanford.edu/projects/histwords/
#' 
#' @examples
#' 
#' \dontrun{
#' 
#'
#' ## download the model (once per machine)
#' download_pretrained("vecs_sgns300_googlengrams_histwords_de")
#' 
#' ## load the model each session
#' data("vecs_sgns300_googlengrams_histwords_de")
#' 
#' ## check dims
#' length(vecs_sgns300_googlengrams_histwords_de) == 20L
#' dim(vecs_sgns300_googlengrams_histwords_de[[1]]) == c(100000, 300)
#' 
#' }
#'
NULL


#' 30k diachronic Chinese-language SGNS word embeddings, 5 decades, Google Books corpus
#'
#' 30 thousand SGNS embeddings from the HistWords project,
#' trained on the Google Books N-Gram corpus
#' (all Chinese) divided into decades.
#' This is a list of 5 elements, in which every element is an
#' embedding matrix associated with a given decade, 1950-1990.
#' Each matrix is30 thousand vectors (rows) and 300 dimensions
#' (columns). Note that each embedding has the same vocabulary,
#' but when words do not appear in a given decade they
#' appear as rows with only zero values.
#'
#' @references
#' Hamilton, William L., Jure Leskovec, and Dan Jurafsky. 2016.
#' "Diachronic Word Embeddings Reveal Statistical Laws of Semantic Change."
#' Pp. 1489--1501 in Proceedings of the 54th Annual Meeting
#' of the Association for Computational Linguistics.
#'
#' @docType data
#' @name vecs_sgns300_googlengrams_histwords_zh
#' @keywords datasets
#' @format A list of 5 matrices
#' @source https://nlp.stanford.edu/projects/histwords/
#' 
#' @examples
#' 
#' \dontrun{
#' 
#'
#' ## download the model (once per machine)
#' download_pretrained("vecs_sgns300_googlengrams_histwords_zh")
#' 
#' ## load the model each session
#' data("vecs_sgns300_googlengrams_histwords_zh")
#' 
#' ## check dims
#' length(vecs_sgns300_googlengrams_histwords_zh) == 5L
#' dim(vecs_sgns300_googlengrams_histwords_zh[[1]]) == c(29701, 300)
#' 
#' }
#'
NULL


#' 75k diachronic English-language SVD word embeddings, 20 decades, Google Books corpus
#'
#' 75 thousand SVD embeddings from the HistWords project,
#' trained on the Google Books N-Gram corpus
#' (all English) divided into decades. 
#' This is a list of 20 elements, in which every element is an 
#' embedding matrix associated with a given decade, 1800-1990. 
#' Each matrix is 75 thousand vectors (rows) and 300 dimensions
#' (columns). Note that each embedding has the same vocabulary, 
#' but when words do not appear in a given decade they
#' appear as rows with only zero values.
#'
#' @references
#' Hamilton, William L., Jure Leskovec, and Dan Jurafsky. 2016.
#' "Diachronic Word Embeddings Reveal Statistical Laws of Semantic Change."
#' Pp. 1489--1501 in Proceedings of the 54th Annual Meeting 
#' of the Association for Computational Linguistics.
#'
#' @docType data
#' @name vecs_svd300_googlengrams_histwords
#' @keywords datasets
#' @format A list of 20 matrices
#' @source https://nlp.stanford.edu/projects/histwords/
#' 
#' @examples
#' 
#' \dontrun{
#' 
#'
#' ## download the model (once per machine)
#' download_pretrained("vecs_svd300_googlengrams_histwords")
#' 
#' ## load the model each session
#' data("vecs_svd300_googlengrams_histwords")
#' 
#' ## check dims
#' length(vecs_svd300_googlengrams_histwords) == 20L
#' dim(vecs_svd300_googlengrams_histwords[[1]]) == c(75682, 300)
#' 
#' }
#'
NULL


#' 79k diachronic English-language SNGS word embeddings, 12 decades, British News corpus
#'
#' 79 thousand SGNS embeddings from Pedrazzini and McGillivray,
#' trained on a corpus of 19th century British newspapers divided into decades. 
#' This is a list of 12 elements, in which every element is an 
#' embedding matrix associated with a given decade, 1800-1910. 
#' Each matrix is 79 thousand vectors (rows) and 200 dimensions
#' (columns). Note that each embedding has the same vocabulary, 
#' but when words do not appear in a given decade they
#' appear as rows with only zero values.
#'
#' @references
#' Pedrazzini, Nilo & Barbara McGillivray. 2022. Diachronic word embeddings 
#' from 19th-century British newspapers [Data set]. Zenodo.
#' \doi{10.5281/zenodo.7181682}

#' @docType data
#' @name vecs_sgns200_british_news
#' @keywords datasets
#' @format A list of 12 matrices
#' @source https://zenodo.org/records/7181682
#' 
#' @examples
#' 
#' \dontrun{
#' 
#'
#' ## download the model (once per machine)
#' download_pretrained("vecs_sgns200_british_news")
#' 
#' ## load the model each session
#' data("vecs_sgns200_british_news")
#' 
#' ## check dims
#' length(vecs_sgns200_british_news) == 12L
#' dim(vecs_sgns200_british_news[[1]]) == c(78879, 200)
#' 
#' }
#'
NULL