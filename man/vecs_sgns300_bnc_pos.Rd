% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/extdata.R
\docType{data}
\name{vecs_sgns300_bnc_pos}
\alias{vecs_sgns300_bnc_pos}
\title{160k English-language SGNS word embeddings trained on the British National Corpus}
\format{
A matrix of 163,473 rows and 300 columns
}
\source{
http://vectors.nlpl.eu/repository/
}
\description{
SGNS embeddings trained on the British National Corpus. Each term is also
tagged with part-of-speech, e.g. "hyperventilation_NOUN"
}
\references{
Tomas Mikolov, Kai Chen, Greg Corrado, and Jeffrey Dean. 2013.
"Efficient Estimation of Word Representations in Vector Space.""
In Proceedings of Workshop at ICLR
}
\keyword{datasets}
